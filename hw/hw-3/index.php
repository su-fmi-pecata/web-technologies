<?php

if ($_SERVER["REQUEST_METHOD"] != "POST") {
    exit("BadReqest: Method is not POST");
}

function getStringLengthErrors($str, $min, $max, $key) {
    $str = trim($str);
    $len = mb_strlen($str, 'UTF-8');

    if ($len == 0 ) {
        return "$key е задължително поле";
    }

    if ($min != NULL && $len < $min) {
        return "$key е твърде късо. Минимална дължина = $min";
    }

    if ($max != NULL && $len > $max) {
        return "$key е твърде дълго. Максимална дължина = $max";
    }

    return NULL;
}


function getNameErrors($name) {
    return getStringLengthErrors($name, 2, 150, "Името");
}

function getTeacherErrors($teacher) {
    return getStringLengthErrors($teacher, 3, 200, "Името на преподавателя");
}

function getDescriptionErrors($description) {
    return getStringLengthErrors($description, 10, NULL, "Описанието");
}

function getGroupErrors($group) {
    if (strlen($group) == 0) {
        return "Групата е задължително поле";
    }

    $availableGroups = ['М', 'ПМ', 'ОКН', 'ЯКН'];
    if (array_search($group, $availableGroups)) {
        return NULL;
    }

    return "Невалидна стойност '$group'. Трябва да е някоя измежду ['М', 'ПМ', 'ОКН', 'ЯКН']";
}

function getCreditsErrors($credits) {
    if (filter_var($credits, FILTER_VALIDATE_INT, array("options" => array("min_range"=>0))) === false) {
        return "Кредитите трябва да са цяло положително число";
    }

    return NULL;
}


$json = file_get_contents('php://input');
$data = json_decode($json);

$errors = [
    "name" => getNameErrors($data->name),
    "teacher" => getTeacherErrors($data->teacher),
    "description" => getDescriptionErrors($data->description),
    "group" => getGroupErrors($data->group),
    "credits" => getCreditsErrors($data->credits),
];

$errors = array_filter($errors, 'strlen');

$errorsCount = count($errors);
$result = [
    "success" => $errorsCount == 0
];

if ($errorsCount > 0) {
    $result["errors"] = $errors;
}

header('Content-Type: application/json; charset=utf-8');
echo json_encode($result);
exit;
?>