const initCodeSnippets = () => {
    const snippets = document.querySelectorAll(".code-snippet[accessors]");
    for (s of snippets) {
        const accessorsAttribute = s.getAttribute("accessors");
        const accessors = accessorsAttribute.split('.');
        let data = window.codeSnippets;
        accessors.forEach(element => {
            data = data[element];
        });

        const html = document.createElement("pre");
        html.innerHTML = data;
        s.appendChild(html);
    }
}

window.codeSnippets = {};