const initTableOfContents = () => {
    const tableOfContents = document.getElementById("table-of-contents");
    const sections = document.getElementsByTagName("section");

    const tableCells = 37;

    const createCell = ({ html, colspan }) => {
        const td = document.createElement("td");
        td.innerHTML = html || '';
        if (colspan) {
            td.setAttribute("colspan", colspan);
        }
        return td;
    };

    const createRow = ({ pageId }) => {
        const row = document.createElement("tr");
        if (pageId) {
            row.onclick = (e) => {
                window.location.href = "#";
                window.location.href = "#" + pageId;
            }
        }
        return row;
    };

    const setupColumns = () => {
        const thead = document.createElement("thead");
        const row = createRow({});
        for (let i = 0; i < tableCells; i++) {
            row.appendChild(createCell({}));
        }
        thead.appendChild(row);
        tableOfContents.appendChild(thead);
    };

    setupColumns();

    let pageNumber = 1;
    for (s of sections) {
        const pageId = "page-number-" + pageNumber;
        if (!s.classList.contains("ignore-table-of-contents")) {
            const pageCell = createCell({ html: pageNumber });
            const headers = s.querySelectorAll("h1,h2,h3,h4,h5,h6");
            for (h of headers) {
                const row = createRow({ pageId });
                const hNumber = parseInt(h.localName.substr(1)) - 1;
                if (hNumber > 0) {
                    row.appendChild(createCell({ colspan: hNumber }));
                }
                row.appendChild(createCell({ html: h.innerHTML, colspan: tableCells - hNumber - 1 }));
                row.appendChild(pageCell.cloneNode(true));
                tableOfContents.appendChild(row);
            }
        }
        s.setAttribute("id", pageId);
        pageNumber++;
    }
};