window.codeSnippets.mixins = Object.freeze({

    less: `.a, #b {
    color: red;
}
.mixin-class {
    .a();
}
.mixin-id {
    #b();
}

/* --------------------------- */

.my-hover-mixin() {
    &:hover {
        border: 1px solid red;
    }
}
button {
    .my-hover-mixin();
}`,

    lessResultCss: `.a, #b {
    color: red;
}
.mixin-class {
    color: red;
}
.mixin-id {
    color: red;
}

/* --------------------------- */

button:hover {
    border: 1px solid red;
}`,

sass: `@mixin theme($theme: DarkGray)
  background: $theme
  box-shadow: 0 0 1px rgba($theme, .25)
  color: #fff

.info
  @include theme

.alert
  @include theme($theme: DarkRed)

.success
  @include theme($theme: DarkGreen)
`,
sassResultCss: `.info {
  background: DarkGray;
  box-shadow: 0 0 1px rgba(169, 169, 169, 0.25);
  color: #fff;
}

.alert {
  background: DarkRed;
  box-shadow: 0 0 1px rgba(139, 0, 0, 0.25);
  color: #fff;
}

.success {
  background: DarkGreen;
  box-shadow: 0 0 1px rgba(0, 100, 0, 0.25);
  color: #fff;
}`,

sassExtends: `.circle
  border: 1px solid #ccc
  border-radius: 50px
  overflow: hidden

.avatar
  @extend .circle`,

lessExtends: `.circle {
  border: 1px solid #ccc;
  border-radius: 50px;
  overflow: hidden;
}
 
.avatar:extend(.circle) {}`,
});

