window.codeSnippets.nestedSelectors = Object.freeze({

    less: `.button {
    color: blue;
    &:hover {
        color: green;
    }
    &-ok {
        background-image: url("ok.png");
    }
    &-cancel {
        background-image: url("cancel.png");
    }
    
    &-custom {
        background-image: url("custom.png");
    }
    & + & {
        color: red;
    }
    .gosho {
        &, &to {
            color: cyan;
        }
        .pesho & {
            color: purple;
        }
    }
}`,

    lessResultCss: `.button {
    color: blue;
}
.button:hover {
    color: green;
}      
.button-ok {
    background-image: url("ok.png");
}
.button-cancel {
    background-image: url("cancel.png");
}
.button-custom {
    background-image: url("custom.png");
}
.button + .button {
    color: red;
}
.button .gosho,
.button .goshoto {
  color: cyan;
}
.pesho .button .gosho {
    color: purple;
}`,

sass: `nav
  ul
    margin: 0
    padding: 0
    list-style: none
  
  li
    display: inline-block
  
  a
    display: block
    padding: 6px 12px
    text-decoration: none`,

sassResultCss: `nav ul {
  margin: 0;
  padding: 0;
  list-style: none;
}
nav li {
  display: inline-block;
}
nav a {
  display: block;
  padding: 6px 12px;
  text-decoration: none;
}`,

sassProperties: `nav 
  margin: 50px auto 0
  width: 788px
  height: 45px
  ul
    padding: 0
    margin: 0
  border:
    style: solid
    left:
      width: 4px
      color: #333333
    right:
      width: 2px
      color: #000000
`,

sassPropertiesResultCss: `nav {
  margin: 50px auto 0;
  width: 788px;
  height: 45px;
  border-style: solid;
  border-left-width: 4px;
  border-left-color: #333333;
  border-right-width: 2px;
  border-right-color: #000000;
}
nav ul {
  padding: 0;
  margin: 0;
}`,
});

