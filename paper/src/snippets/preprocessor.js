window.codeSnippets.preprocessor = Object.freeze({

    expanded: `.styled-element {
    background: red;
}`,

    minified: `.styled-element{background:red;}`,
});

