window.codeSnippets.variables = Object.freeze({

    less: `/* Declarations */
@my-selector: red;<br />
@link-color: #428bca;
@link-color-hover: darken(@link-color, 10%);
@images: "../img";

/* Usage */
a, .link { 
    color: @link-color;
}
a:hover {
    color: @link-color-hover;
}
.@{my-selector} {
    background: url("@{images}/white-sand.png");
}
    `,

    css: `/* Declarations */
:root {
    --my-selector: red;
    --link-color: #428bca;
    --link-color-hover: #3071a9;
    --images: "../img";
} 

/* Usage */
a, .link {  
    color: var(--link-color); 
} 
a:hover { 
    color: var(--link-color-hover); 
} 
/* not possible :/ */
`,
    sass: `$base-color: #c6538c
$border-dark: rgba($base-color, 0.88)

.alert
    border: 1px solid $border-dark
`,
    sass_css: `.alert {
    border: 1px solid rgba(198, 83, 140, 0.88);
}
`
});

